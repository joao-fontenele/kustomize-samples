# Kustomize Samples

This is a kustomize sandbox with samples of common patches I found while managing kubernetes.

The interesting parts are in the [overlays](./overlays) directory. To build every overlay simply run:

```bash
make kustomize
```

This will generate an `out.yaml` in each overlay directory. There's an overlay without any patch that you can use to
take make clear what changed in comparison with other overlays. To check you could use meld:

```bash
meld overlays/no-patch/out.yaml overlays/json-patch-remove/out.yaml
```
