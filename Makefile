OVERLAY_DIRS := $(wildcard ./overlays/*/)
.PHONY: kustomize
kustomize: $(OVERLAY_DIRS)
	@for dir in $^; do \
		echo "applying overlay in $${dir}" ; \
		kustomize build --enable-alpha-plugins --enable-exec --enable-helm $${dir} -o $${dir}out.yaml ; \
	done
